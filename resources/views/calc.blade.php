@extends('layout')

@section('content')

		
	{{-- タイトル表示 --}}
	<div class="container">
		<div class="h2">
			電卓
		</div>
	</div>
	
	{{-- 計算フォーム表示 --}}
	<div class="container">
		<div class="h3">
			計算
		</div>
		<form action="{{ url('/') }}" method="POST" class="form-horizontal" >
			{{csrf_field() }}
			<div id="calc">
				<div class="form-group col-sm-12">
					<div class="row">
						<div class="col-sm-6">
							<input v-model="formula" class="form-control input-lg" name="formula" placeholder="数式を入力">
						</div>
						<div class="col-sm-6 h3" v-cloak> = @{{ answer }} </div>
						{{-- formパラメータの計算結果 --}}
						<input type="hidden" name="answer" v-bind:value="answer">
					</div>
					<div class="form-group col-sm-3">
						<button type="submit" class="btn btn-primary" v-bind:disabled="!isFormula" style="margin-top:10px;">履歴に保存</button>
					</div>
				</div>
			</div>
		</form>
	</div>

	{{-- 履歴表示 --}}
	<div class="container">
		@if($histories->isEmpty())
			<p class="panel-info">履歴が登録されていません</p>
		@else
			{{-- もしたすくがあれば(if) --}}
			<div class="h3">履歴</div>
			<div class="panel panel-default">
				<div class="panel-body" id="viewHistory" style="font-family: 'Segoe UI';font-size:'1.2'">
					<table class="table table-striped">
						<tbody>
						@foreach ($histories as $history)
							<tr>
								<td class="table-text">
									<button v-on:click="retriveFormula({{$history->id}},'{{$history->formula}}')" class="btn btn-default">
										{{ $history->formula . ' = ' . strval(doubleval($history->answer))}}
									</button>
								</td>
								<td>
									<form action="{{url('/'.$history->id) }}" method="POST">
										{{ csrf_field() }}
										{{ method_field('DELETE') }}
										<button type="submit" id="delete-history-{{ $history->id }}" class="btn btn-danger">削除</button>
									</form>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		@endif
	</div>

	{{--<script>--}}
	{{--vue--}}
    {{--</script>--}}
	<script type="text/javascript" src="js/app.js"></script>
@endsection