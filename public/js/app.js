var app = new Vue({
	el: '#calc',
	data: {
		formula: '',
		isFormula: false
	},
	computed: {
	/* 数式を解析して計算する関数
	 * 入力:数式(this.formula)
	 * 出力:計算結果
	 * */
		answer: function() {
			var answer = 0;
			var temp_ans = 0;
			var next_operator = "";
			if(!this.formula){
				this.isFormula = false;
				return "数式を入力してください";
			}
			//入力値を数値、演算子に分け、inputArrayに格納
			var input = this.formula.replace(/\s/g,"");
			var inputArray = input.split(/([^\d\.])/);

			//入力値のチェック
			for(var i = 0; i < inputArray.length; i++) {
				//奇数番目の要素が演算子であることのチェック
				if(i % 2 !== 0){
					//演算子でないとき
					if(inputArray[i] !== "+" &&
						inputArray[i] !== "-" &&
						inputArray[i] !== "*" &&
						inputArray[i] !== "/" ){
						this.isFormula = false;
						return "正しい数式を入力してください"
					}
				//偶数番目の要素が数字であることのチェック
				}else{
					//数字でないとき
					if(isNumber(inputArray[i]) == false){
						this.isFormula = false;
						return "正しい数式を入力してください"
					}
				}
			}

			//×、÷の計算
			for (i = 0; i < inputArray.length; i++) {
				if (inputArray[i] === '*') {
					inputArray[i-1] = Number(inputArray[i-1]) * Number(inputArray[i+1]);
					//演算子と次の数値を配列から削除
					inputArray.splice(i,2);
					//次の演算子を見る
					i = i - 1;
				}
				else if (inputArray[i] === '/') {
					inputArray[i-1] = Number(inputArray[i-1]) / Number(inputArray[i+1]);
					//演算子と次の数値を配列から削除
					inputArray.splice(i,2);
					//次の演算子を見る
					i = i - 1;
				}
			}
			for (i = 0; i < inputArray.length; i++) {
				if (inputArray[i] === '+') {
					inputArray[i-1] = Number(inputArray[i-1]) + Number(inputArray[i+1]);
					//演算子と次の数値を配列から削除
					inputArray.splice(i,2);
					//次の演算子を見る
					i = i - 1;
				}
				else if (inputArray[i] === '-') {
					inputArray[i-1] = Number(inputArray[i-1]) - Number(inputArray[i+1]);
					//演算子と次の数値を配列から削除
					inputArray.splice(i,2);
					//次の演算子を見る
					i = i - 1;
				}
			}

			this.isFormula = true;
			return inputArray[0];

			//入力値が数字かチェック
			function isNumber(numVal){
				// チェック条件パターン
				var pattern = /^[-]?([1-9]\d*|0)(\.\d+)?$/;
				// 数値チェック
				return pattern.test(numVal);
			}
		}
	}
})

/**
 * 履歴からテキストフィールドに数式をコピーする
 */
var history = new Vue({
	el: '#viewHistory',
	data: {
	},
	methods: {
		//指定されたidの数式をmodelにうつす
		retriveFormula: function(id,formula) {
			app.formula = formula;
		}
	}
})