<?php

namespace App\Http\Controllers;
use App\History;

use Illuminate\Http\Request;

class HistoryController extends Controller
{
	/**
	 * 計算フォームと履歴を表示する。
	 * 履歴が存在しなければその旨のメッセージを表示。
	 *
	 * @param Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index(Request $request) {
		$histories = History::get()->sortByDesc('id');
//dd($histories);
		return view('calc',[
				'histories' => $histories,
		]);
	}
	
	/**
	 * パラメータ「formula」「answer」を
	 * レコードとしてDBに格納する。
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function add(Request $request)
	{
		$this->validate($request, [
			//数値が入っていることのチェック
				'answer' => 'required|numeric'
		]);
		History::create([
				'formula' => $request->formula,
				'answer' => $request->answer,
		]);
		\Session::flash('flash_message', '履歴を保存しました');
		return redirect('/');
	}
	
	public function delete(Request $request,History $history)
	{
		\Session::flash('flash_message', '履歴を削除しました。');  //
		$history->delete();
		
		return redirect('/');
	}
}
